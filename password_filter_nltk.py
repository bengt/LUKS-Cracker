import pickle
import nltk

words = nltk.corpus.words.words() + nltk.corpus.stopwords.words('english') + nltk.corpus.stopwords.words('german') + ["cage"]

# collect filtered passwords from dumpfile using nltk
filtered_passwords_0 = []
with open("passwords_filtered_pwquality_0.pkl", "rb") as dumpfile:
    passwords = pickle.load(dumpfile)
    for password in passwords:
        password_start = password[:4]
        good = True  # in dubio pro reo
        if password_start in words:
            good = False
        if good:
            filtered_passwords_0.append(password)
with open("passwords_filtered_nltk_0.pkl", 'wb') as dumpfile_0:
    pickle.dump(filtered_passwords_0, dumpfile_0)

filtered_passwords_1 = []
with open("passwords_filtered_pwquality_1.pkl", "rb") as dumpfile:
    passwords = pickle.load(dumpfile)
    for password in passwords:
        password_start = password[:4]
        good = True  # in dubio pro reo
        if password_start in words:
            good = False
        if good:
            filtered_passwords_1.append(password)
with open("passwords_filtered_nltk_1.pkl", 'wb') as dumpfile_1:
    pickle.dump(filtered_passwords_1, dumpfile_1)
