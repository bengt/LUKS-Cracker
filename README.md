# LUKS-Cracker

Password bruteforcing for LUKS

## Disclaimer

> This repository documents my failed attempt to recover a forgotten password.
> Do not attempt to use this without modification, it will not work for you as-is.
> I will not help you with using this as I can not verify the legitimacy of you accessing the data in question.

## Install Requirements

    sudo pip install -r requirements.txt
    
## Running

    pypy password_generator.py
    python password_filter_pwquality.py
    python password_filter_nltk.py
    pypy password_aggregator.py
    sudo python LUKS_opener.py

