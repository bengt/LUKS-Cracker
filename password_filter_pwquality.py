import pickle

import pwquality

# collect filtered passwords from generated passwords using PWQuality
filtered_passwords_0 = []
with open("passwords_generated_0.pkl", "rb") as in_file:
    passwords = pickle.load(in_file)
    for password in passwords:
        try:
            entropy = pwquality.PWQSettings().check(password)
        except pwquality.PWQError as exception:
            print password, exception
            continue
        filtered_passwords_0.append(password)
with open("passwords_filtered_pwquality_0.pkl", "wb") as out_file_0:
    pickle.dump(filtered_passwords_0, out_file_0)
    
filtered_passwords_1 = []
with open("passwords_generated_1.pkl", "rb") as in_file:
    passwords = pickle.load(in_file)
    for password in passwords:
        try:
            entropy = pwquality.PWQSettings().check(password)
        except pwquality.PWQError as exception:
            print password, exception
            continue
        filtered_passwords_1.append(password)
with open("passwords_filtered_pwquality_1.pkl", "wb") as out_file_1:
    pickle.dump(filtered_passwords_1, out_file_1)

