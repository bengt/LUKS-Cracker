import pickle

with open("passwords_filtered_nltk_0.pkl", "rb") as dumpfile_0:    
    with open("passwords_filtered_nltk_1.pkl", "rb") as dumpfile_1:
        passwords = pickle.load(dumpfile_0) + pickle.load(dumpfile_1)
        passwords = list(set(passwords))
        with open("passwords_aggregated.pkl", "wb") as out_file:
            pickle.dump(passwords, out_file)
