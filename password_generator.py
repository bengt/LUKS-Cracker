import itertools
import string
import pickle

alphabet_allowed = string.ascii_lowercase + "3"
alphabet_required = ["w", "g", "e"]
password_length = 8
possible_suffixes = ["5h", "h5"] # need to be same length for now
chars_required = ["", "", "", "", "3", "", "", ""]
impossible_letters = "xyzmnk"

collected_password_candidates_0 = []
collected_password_candidates_1 = []

for index, combination in enumerate(itertools.permutations(alphabet_allowed, (password_length - len(possible_suffixes[0])))):
    # append suffix
    for suffix_index, suffix in enumerate(possible_suffixes):
        password_candidate = "".join(combination) + suffix

        bad = False

        # skip for chars required at a position
        for char_index, char in enumerate(chars_required):
	        if char != "" and password_candidate[char_index] != char:
	            bad = True
	            break

        if bad:
	        break

        # skip for letters required to be in the password
        for char in alphabet_required:
	        if char not in password_candidate:
	            bad = True
	            break

        if bad:
	        break

        # skip for impossible letters
        for char in impossible_letters:
            if char in password_candidate:
                bad = True
                break

        if bad:
	        break

        print(password_candidate)
        if suffix_index:
            collected_password_candidates_1.append(password_candidate)
        else:
            collected_password_candidates_0.append(password_candidate)            
 
    if not index % 100000:
        print "index:", index, "candidates_0:", str(len(collected_password_candidates_0)), "candidates_1:", str(len(collected_password_candidates_1))
        with open("dumpfile_0.pkl", 'wb') as dumpfile_0:
            pickle.dump(collected_password_candidates_0, dumpfile_0)
        with open("dumpfile_1.pkl", 'wb') as dumpfile_1:
            pickle.dump(collected_password_candidates_1, dumpfile_1)

print "index:", index, "candidates_0:", str(len(collected_password_candidates_0)), "candidates_1:", str(len(collected_password_candidates_1))
with open("passwords_generated_0.pkl", 'wb') as dumpfile_0:
    pickle.dump(collected_password_candidates_0, dumpfile_0)
with open("passwords_generated_1.pkl", 'wb') as dumpfile_1:
    pickle.dump(collected_password_candidates_1, dumpfile_1)

