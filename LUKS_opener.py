# -*- coding:utf-8 -*-

from __future__ import division

import os
import pickle

import pexpect

with open("passwords_aggregated.pkl", "r") as in_file:
    passwords = pickle.load(in_file)
    for index, password in enumerate(passwords):
        password = password.rsplit("\n")[0]
        print(index, round(index / len(passwords), 4), password )
        child = pexpect.spawn("cryptsetup luksOpen /dev/sda2 root")
        child.expect("Geben Sie den Passsatz für /dev/sda2 ein:")
        child.sendline(password)
        # 0 => failure, 1 => success
        success = child.expect(["Kein Schlüssel mit diesem Passsatz verfügbar.", "Kann Gerät /dev/sda2 nicht nutzen, da es bereits in Betrieb ist."])
        if success:
            print(password)
            with open("password_successful.txt", "w") as out_file:
                out_file.write(password)
            exit()

